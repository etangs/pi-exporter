# pi-exporter

## Objectives
Export to prometheus stack the most essentiel metrics of a pi server
- [ ] Temperature of CPU
- [ ] CPU usage, and load average
- [ ] Memory usage
- [ ] Disk space and io
- [ ] Number of containers/pods running on the nodes

Ref: 
https://www.cyberciti.biz/faq/linux-find-out-raspberry-pi-gpu-and-arm-cpu-temperature-command/
https://prometheus.io/docs/guides/go-application/
https://pkg.go.dev/github.com/rafacas/sysstats
https://kubernetes.io/docs/tasks/debug/debug-cluster/crictl/

